import { advancedJavaScript, advancedJavaScriptHomeworks } from "./source.js";
import { createElement } from "./modules/functions.js";

const app = document.getElementById("app");
const title = document.querySelector(".title");

const lessonList = createElement("div", "lesson-list show");
app.append(lessonList);

advancedJavaScript.forEach((element, index) => {
  const lesson = createElement("div", "lesson");
  lesson.setAttribute("data-lesson", index + 1);
  const lessonDescription = createElement("div", "lesson__description");
  const lessonNumber = createElement("span", "description__number");
  const lessonTitle = createElement("span", "description__title");
  lessonList.append(lesson);
  lesson.append(lessonDescription);
  lessonDescription.append(lessonNumber, lessonTitle);
  const subLessons = createElement("div", "sublessons");
  const subLessonsWrapper = createElement("div", "sublessons-wrapper");
  lesson.append(subLessonsWrapper);
  subLessonsWrapper.append(subLessons);
  lessonNumber.innerText = element.lesson + ".";
  lessonTitle.innerText = element.lessonName;
  element.lessons.forEach((element, index) => {
    const subLesson = createElement("a", "sublesson");
    subLessons.append(subLesson);
    subLesson.innerText = element.name;
    if (element.url.includes("youtube")) {
      subLesson.innerText = `${element.name} (відео)`;
    }
    if (element.url) {
      subLesson.href = element.url;
      subLesson.target = "_blank";
    }
    if (!element.url) subLesson.href = "#!";
  });
  lessonDescription.addEventListener("click", () => {
    if (+getComputedStyle(subLessonsWrapper).paddingBottom.slice(0, -2) === 0) {
      subLessonsWrapper.style.paddingBottom =
        +getComputedStyle(subLessons).height.slice(0, -2) + 10 + "px";
    } else if (
      +getComputedStyle(subLessonsWrapper).paddingBottom.slice(0, -2) > 0
    ) {
      subLessonsWrapper.style.paddingBottom = "0px";
    }
  });
});

lessonList.style.top = +getComputedStyle(title).height.slice(0, -2) + 10 + "px";

const homeWorksList = createElement("div", "homework-list");
app.prepend(homeWorksList);

advancedJavaScriptHomeworks.forEach((homework, index) => {
  const homeWork = createElement("div", "homework");
  homeWorksList.append(homeWork);
  homeWork.setAttribute("data-homework", index + 1);
  const homeworkTitle = createElement("span", "homework__title");
  homeworkTitle.innerText = homework.text;
  const homeworkTasks = createElement("div", "homework__tasks");
  const homeworkTasksWrapper = createElement("div", "task-wrapper");
  homeWork.append(homeworkTitle, homeworkTasksWrapper);
  homeworkTasksWrapper.append(homeworkTasks);

  if (homework.theory) {
    const theory = createElement("div", "theory");
    homeworkTasks.append(theory);
    const theoryTitle = createElement("p", "theory-title");
    theoryTitle.innerText = homework.theory.text;
    theory.append(theoryTitle);
    homework.theory.questions.forEach((question, index) => {
      const taskQuestion = createElement("div", "question");
      theory.append(taskQuestion);
      const questionNumber = createElement("span", "quewtion-number");
      questionNumber.innerText = index + 1 + ". ";
      const questionText = createElement("span", "question-text");
      questionText.innerText = question;
      taskQuestion.append(questionNumber, questionText);
    });
  }
  homework.task.forEach((task) => {
    const tas = createElement("p", "task");
    tas.innerText = task;
    homeworkTasks.append(tas);
  });
  homeworkTitle.addEventListener("click", () => {
    if (
      +getComputedStyle(homeworkTasksWrapper).paddingBottom.slice(0, -2) === 0
    ) {
      homeworkTasksWrapper.style.paddingBottom =
        +getComputedStyle(homeworkTasks).height.slice(0, -2) + 10 + "px";
    } else if (
      +getComputedStyle(homeworkTasksWrapper).paddingBottom.slice(0, -2) > 0
    ) {
      homeworkTasksWrapper.style.paddingBottom = "0px";
    }
  });
});

title.addEventListener("click", () => {
  if (!lessonList.classList.contains("show")) {
    title.innerText = "Перелік домашніх завдань ->";
  } else if (!homeWorksList.classList.contains("show")) {
    title.innerText = "<- Перелік зайнять";
  }
  homeWorksList.classList.toggle("show");
  lessonList.classList.toggle("show");
});
