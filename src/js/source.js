export const advancedJavaScript = [
  {
    lesson: "Lesson 1",
    lessonName: "Function constructor. this, як контекст виконання функції.",
    lessons: [
      {
        name: "Об'єктно-орієнтований JavaScript: функції конструктори.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_constructor_function/ext_constructor_func.html",
      },
      {
        name: "Докладно про те, як працює це JavaScript.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_this/ext_this.html",
      },
      {
        name: 'Методи об’єкту, "this".',
        url: "https://uk.javascript.info/object-methods",
      },
      {
        name: "When to Use Bind(), Call(), and Apply() in JavaScript.",
        url: "https://betterprogramming.pub/when-to-use-bind-call-and-apply-in-javascript-1ae9d7fa66d5",
      },
      {
        name: "The JavaScript this Keyword.",
        url: "https://www.w3schools.com/js/js_this.asp",
      },
      {
        name: "When to Use Bind(), Call(), and Apply() in JavaScript.",
        url: "https://betterprogramming.pub/when-to-use-bind-call-and-apply-in-javascript-1ae9d7fa66d5",
      },
      {
        name: "this on MDN.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this",
      },
      {
        name: "JS Function Methods call( ), apply( ), and bind( ).",
        url: "https://www.youtube.com/watch?v=uBdH0iB1VDM",
      },
      {
        name: "Object Oriented JavaScript Tutorial #1 - Introduction.",
        url: "https://www.youtube.com/watch?v=4l3bTDlT6ZI",
      },
      {
        name: "JavaScript this Keyword.",
        url: "https://www.youtube.com/watch?v=gvicrj31JOM",
      },
    ],
  },
  {
    lesson: "Lesson 2",
    lessonName: "Прототипне успадкування. ES6 класи.",
    lessons: [
      {
        name: "Навіщо нам ОВП і що це таке.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_oop_purpose/ext_oop_purpose.html",
      },
      {
        name: "Фундаментальні принципи об&#39;єктно-орієнтованого програмування JavaScript.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_basicOOP/ext_basicOOP.html",
      },
      {
        name: "Клас: базовий синтаксис.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_basic_class_syntax/ext_basic_class_syntax.html",
      },
      {
        name: "Класи в ES6.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_es6_classes/ext_es6_classes.html",
      },
      {
        name: "OOP Explained with Examples.",
        url: "https://www.freecodecamp.org/news/how-javascript-implements-oop/",
      },
      {
        name: "Успадкування через прототипи.",
        url: "https://uk.javascript.info/prototype-inheritance",
      },
      {
        name: "Наслідування класу.",
        url: "https://uk.javascript.info/class-inheritance",
      },
      {
        name: "Classes.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes",
      },
      {
        name: "constructor.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes/constructor",
      },
      {
        name: "JavaScript Classes Tutorial.",
        url: "https://www.youtube.com/watch?v=2ZphE5HcQPQ",
      },
      {
        name: "Object Oriented JavaScript Tutorial #4 - Classes.",
        url: "https://www.youtube.com/watch?v=Ug4ChzopcE4",
      },
    ],
  },
  {
    lesson: "Lesson 3",
    lessonName: "ES6 класи - практика.",
    lessons: [
      {
        name: "Доступ до тесту в тебе закритий ):",
        url: "",
      },
    ],
  },
  {
    lesson: "Lesson 4",
    lessonName: "Замикання. Обробка помилок (try...catch).",
    lessons: [
      {
        name: "Замикання.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_closures/ext_closures.html",
      },
      {
        name: 'Обробка помилок "try..catch".',
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_try_catch/ext_try_catch.html",
      },
      {
        name: "Error на MDN.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error",
      },
      {
        name: "Throw on MDN.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/throw",
      },
      {
        name: "Try, catch on MDN.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/try...catch",
      },
      {
        name: "Learn Closures In 7 Minutes.",
        url: "https://www.youtube.com/watch?v=3a0I8ICR1Vg",
      },
      {
        name: "try, catch, finally, throw - error handling in JavaScript.",
        url: "https://www.youtube.com/watch?v=cFTFtuEQ-10",
      },
    ],
  },
  {
    lesson: "Lesson 5",
    lessonName:
      "Destructuring of objects and arrays, spread and rest operators.",
    lessons: [
      {
        name: "Деструктуризація в ES6.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_destucturing/ext_destructuring.html",
      },
      {
        name: "Оператор spread та rest параметри в JavaScript.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_spread_rest/ext_spread_rest.html",
      },
      {
        name: "Destructuring assignment.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment",
      },
      {
        name: "JavaScript Object Destructuring.",
        url: "https://www.javascripttutorial.net/es6/javascript-object-destructuring/",
      },
      {
        name: "The Ultimate Guide to the ES2020 Nullish Coalescing Operator.",
        url: "https://blog.bitsrc.io/the-ultimate-guide-to-the-es2020-nullish-coalescing-operator-231d2b64dfde",
      },
      {
        name: "JS Destructuring in 100 Seconds.",
        url: "https://www.youtube.com/watch?v=UgEaJBz3bjY",
      },
      {
        name: "Why Is Array/Object Destructuring So Useful And How To Use It.",
        url: "https://www.youtube.com/watch?v=NIq3qLaHCIs",
      },
    ],
  },
  {
    lesson: "Lesson 6",
    lessonName: "Заняття на самостійну обробку: Testing Javascript code.",
    lessons: [
      {
        name: "Тестування JavaScript коду.",
        url: "https://www.youtube.com/watch?v=e4irdBwRZAk&t=0s",
      },
      {
        name: "Jest Tutorial для початківців.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_jest/ext_jest.html",
      },
      {
        name: "Jest документація.",
        url: "https://jestjs.io/uk/docs/getting-started",
      },
      {
        name: "Jest Tutorial – JavaScript Unit Testing Using Jest Framework.",
        url: "https://www.softwaretestinghelp.com/jest-testing-tutorial/",
      },
      {
        name: "An Overview of JavaScript Testing in 2022.",
        url: "https://medium.com/welldone-software/an-overview-of-javascript-testing-7ce7298b9870",
      },
      {
        name: "Jest Crash Course - Unit Testing in JavaScript.",
        url: "https://www.youtube.com/watch?v=7r4xVDI2vho",
      },
    ],
  },
  {
    lesson: "Lesson 7",
    lessonName: "Promise.",
    lessons: [
      {
        name: "Проміси.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_promise/ext_promise.html",
      },
      {
        name: "Проміси на прикладах із життя.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_promises_with_examples/ext_promises_with_examples.html",
      },
      {
        name: "JavaScript Promise Tutorial: Resolve, Reject, and Chaining in JS and ES6.",
        url: "https://www.freecodecamp.org/news/javascript-es6-promises-for-beginners-resolve-reject-and-chaining-explained/",
      },
      {
        name: "Promise on MDN.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise",
      },
      {
        name: "JavaScript Promises In 10 Minutes.",
        url: "https://www.youtube.com/watch?v=DHvZLI7Db8E",
      },
      {
        name: "Async JS Crash Course - Callbacks, Promises, Async Await.",
        url: "https://www.youtube.com/watch?v=PoRJizFvM7s",
      },
    ],
  },
  {
    lesson: "Lesson 8",
    lessonName: "AJAX, fetch. axios. JSON parse, stringify.",
    lessons: [
      {
        name: "Простими словами про «фронтенд» і «бекенд»: що це таке і як вони взаємодіють.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_front-end_and_back-end/ext_front-end_and_back-end.html",
      },
      {
        name: "Вступ до JSON.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_json_intro/ext_json_intro.html",
      },
      {
        name: "Fetch.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_fetch/ext_fetch.html",
      },
      {
        name: "Frontend та backend.",
        url: "https://uk.wikipedia.org/wiki/Front_end_%D1%82%D0%B0_back_end",
      },
      {
        name: "JSON - Introduction.",
        url: "https://www.w3schools.com/js/js_json_intro.asp",
      },
      {
        name: "Fetch (ще одни).",
        url: "https://javascript.info/fetch",
      },
      {
        name: "Fetch API.",
        url: "https://javascript.info/fetch-api",
      },
      {
        name: "Клієнт-серверна архітектура.",
        url: "https://uk.wikipedia.org/wiki/%D0%9A%D0%BB%D1%96%D1%94%D0%BD%D1%82-%D1%81%D0%B5%D1%80%D0%B2%D0%B5%D1%80%D0%BD%D0%B0_%D0%B0%D1%80%D1%85%D1%96%D1%82%D0%B5%D0%BA%D1%82%D1%83%D1%80%D0%B0",
      },
      {
        name: "Fetching data from the server.",
        url: "https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Fetching_data",
      },
      {
        name: "Learn Fetch API In 6 Minutes.",
        url: "https://www.youtube.com/watch?v=cuEtnrL9-H0",
      },
      {
        name: "Learn JSON in 10 Minutes.",
        url: "https://www.youtube.com/watch?v=iiADhChRriM",
      },
      {
        name: "Fetch API Introduction.",
        url: "https://www.youtube.com/watch?v=Oive66jrwBs",
      },
    ],
  },
  {
    lesson: "Lesson 9",
    lessonName:
      "Типи запитів (POST, PUT, DELETE). Заголовки запитів (Content-Type).",
    lessons: [
      {
        name: "Введення в REST API - RESTful веб-сервіси.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_rest_API_intro/ext_rest_API_intro.html",
      },
      {
        name: "Методи HTTP запиту.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_http_request_methods/ext_http_request_methods.html",
      },
      {
        name: "REST.",
        url: "https://uk.wikipedia.org/wiki/REST",
      },
      {
        name: "HTTP.",
        url: "https://uk.wikipedia.org/wiki/HTTP",
      },
      {
        name: "Cross-Origin Resource Sharing.",
        url: "https://uk.wikipedia.org/wiki/Cross-Origin_Resource_Sharing",
      },
      {
        name: "Request header.",
        url: "https://developer.mozilla.org/en-US/docs/Glossary/Request_header",
      },
      {
        name: "HTTP header.",
        url: "https://developer.mozilla.org/en-US/docs/Glossary/HTTP_header",
      },
      {
        name: "Що таке API? Розуміння API для початківців.",
        url: "https://www.youtube.com/watch?v=P_4X-IDrosc",
      },
      {
        name: "What Is A RESTful API? Explanation of REST &amp; HTTP.",
        url: "https://www.youtube.com/watch?v=Q-BpqyOT3a8",
      },
      {
        name: "HTTP Methods - GET, POST, PUT, DELETE, HEAD, OPTIONS, PATCH.",
        url: "https://www.youtube.com/watch?v=Rcw0s_qLfW0",
      },
    ],
  },
  {
    lesson: "Lesson 10",
    lessonName: "async/await. Цикл подій. Асинхронність у JS.",
    lessons: [
      {
        name: "Async/await.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_async_await/ext_async_await.html",
      },
      {
        name: "Повне розуміння синхронного та асинхронного JavaScript з Async/Await.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_full_understanding_async_await/ext_full_understanding_async_await.html",
      },
      {
        name: "Пояснення роботи EventLoop JavaScript.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_eventLoop/ext_eventLoop.html",
      },
      {
        name: "async function.",
        url: "https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/async_function",
      },
      {
        name: "JavaScript Async.",
        url: "https://www.w3schools.com/js/js_async.asp",
      },
      {
        name: "Modern Asynchronous JavaScript with Async and Await.",
        url: "https://nodejs.dev/learn/modern-asynchronous-javascript-with-async-and-await",
      },
      {
        name: "The event loop.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop",
      },
      {
        name: "JavaScript Async Await.",
        url: "https://www.youtube.com/watch?v=V_Kr9OSfDeU",
      },
      {
        name: "What the heck is the event loop anyway?",
        url: "https://www.youtube.com/watch?v=8aGhZQkoFbQ",
      },
    ],
  },
  {
    lesson: "Lesson 11",
    lessonName: "ES6 модулі.",
    lessons: [
      {
        name: "Глибоке занурення в ES-модулі у картинках.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_deep_dip_in_es-modules/ext_deep_dip_in_es-modules.html",
      },
      {
        name: "Вступ до модулів.",
        url: "https://uk.javascript.info/modules-intro",
      },
      {
        name: "Експорт та імпорт.",
        url: "https://uk.javascript.info/import-export",
      },
      {
        name: "Understanding ES6 Modules via Their History.",
        url: "https://www.sitepoint.com/understanding-es6-modules-via-their-history/",
      },
      {
        name: "JavaScript modules.",
        url: "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules",
      },
      {
        name: "JavaScript ES6 Modules.",
        url: "https://www.youtube.com/watch?v=cRHQNNcYf6s",
      },
      {
        name: "JavaScript Modules in 100 Seconds.",
        url: "https://www.youtube.com/watch?v=qgRUr-YUk1Q",
      },
    ],
  },
  {
    lesson: "Lesson 12",
    lessonName: "Bootstrap.",
    lessons: [
      {
        name: "Bootstrap.",
        url: "https://dan-it.gitlab.io/fe-book-ua/advanced_javascript/ext_bootstrap/ext_bootstrap.html",
      },
      {
        name: "Bootstrap 5 Tutorial.",
        url: "https://www.w3schools.com/bootstrap5/",
      },
      {
        name: "Official docs.",
        url: "https://getbootstrap.com/docs/5.0/getting-started/introduction/",
      },
      {
        name: "Bootstrap Tutorial.",
        url: "https://www.tutorialrepublic.com/twitter-bootstrap-tutorial/",
      },
      {
        name: "Узнаем Bootstrap 4 за 30 минут, создавая лендинг.",
        url: "https://medium.com/@stasonmars/%D1%83%D0%B7%D0%BD%D0%B0%D0%B5%D0%BC-bootstrap-4-%D0%B7%D0%B0-30-%D0%BC%D0%B8%D0%BD%D1%83%D1%82-%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%B2%D0%B0%D1%8F-%D0%BB%D0%B5%D0%BD%D0%B4%D0%B8%D0%BD%D0%B3-d268d52d6c84",
      },
      {
        name: "CSS фреймворки. Bootstrap.",
        url: "https://www.youtube.com/watch?v=M_G0JctH0Gg",
      },
      {
        name: "Bootstrap 5 Crash Course Tutorial #1.",
        url: "https://www.youtube.com/watch?v=O_9u1P5YjVc",
      },
    ],
  },
];

export const advancedJavaScriptHomeworks = [
  {
    text: "Домашнє завдання 1 (після зайняття 1).",
    theory: {
      text: "Теоретичні питання",
      questions: [
        "Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.",
        "Для чого потрібно викликати super() у конструкторі класу-нащадка?",
      ],
    },
    task: [
      "Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.",
      "Створіть гетери та сеттери для цих властивостей.",
      "Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).",
      "Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.",
      "Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.",
    ],
  },
  {
    text: "Домашнє завдання 2 (після зайняття 4).",
    theory: {
      text: "Теоретичні питання",
      questions: [
        "Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.",
      ],
    },
    task: [
      `
      const books = [
        { 
          author: "Люсі Фолі",
          name: "Список запрошених",
          price: 70 
        }, 
        {
         author: "Сюзанна Кларк",
         name: "Джонатан Стрейндж і м-р Норрелл",
        }, 
        { 
          name: "Дизайн. Книга для недизайнерів.",
          price: 70
        }, 
        { 
          author: "Алан Мур",
          name: "Неономікон",
          price: 70
        }, 
        {
         author: "Террі Пратчетт",
         name: "Рухомі картинки",
         price: 40
        },
        {
         author: "Анґус Гайленд",
         name: "Коти в мистецтві",
        }
      ];`,
      "Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).",
      'На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).',
      "Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.",
      "Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.",
    ],
  },
  {
    text: "Домашнє завдання 3 (після зайняття 5).",
    theory: {
      text: "Теоретичні питання",
      questions: [
        "Поясніть своїми словами, як ви розумієте, що таке деструктуризація і навіщо вона потрібна",
        "Усі завдання потрібно виконати, використовуючи синтаксис деструктуризації.",
      ],
    },
    task: [
      `
      Дві компанії вирішили об'єднатись, і для цього їм потрібно об'єднати базу даних своїх клієнтів.

      У вас є 2 масиви рядків, у кожному з них – прізвища клієнтів. Створіть на їх основі один масив, який буде об'єднання двох масивів без повторюваних прізвищ клієнтів.`,
      `
      const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
      const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];`,
      `Завдання 2
      Перед вами массив characters, що складається з об'єктів. Кожен об'єкт описує одного персонажа.
      
      Створіть на його основі масив charactersShortInfo, що складається з об'єктів, у яких є тільки 3 поля - ім'я, прізвище та вік.`,
      `
      const characters = [
        {
          name: "Елена",
          lastName: "Гилберт",
          age: 17, 
          gender: "woman",
          status: "human"
        },
        {
          name: "Кэролайн",
          lastName: "Форбс",
          age: 17,
          gender: "woman",
          status: "human"
        },
        {
          name: "Аларик",
          lastName: "Зальцман",
          age: 31,
          gender: "man",
          status: "human"
        },
        {
          name: "Дэймон",
          lastName: "Сальваторе",
          age: 156,
          gender: "man",
          status: "vampire"
        },
        {
          name: "Ребекка",
          lastName: "Майклсон",
          age: 1089,
          gender: "woman",
          status: "vempire"
        },
        {
          name: "Клаус",
          lastName: "Майклсон",
          age: 1093,
          gender: "man",
          status: "vampire"
        }
      ];
      `,
      `Завдання 3
      У нас є об'єкт' user:`,
      `const user1 = {
        name: "John",
        years: 30
      };`,
      `Напишіть деструктуруюче присвоєння, яке:

      властивість name присвоїть в змінну ім'я
      властивість years присвоїть в змінну вік
      властивість isAdmin присвоює в змінну isAdmin false, якщо такої властивості немає в об'єкті
      Виведіть змінні на екран.`,
      `Завдання 4
      Детективне агентство кілька років збирає інформацію про можливу особистість Сатоши Накамото. Вся інформація, зібрана у конкретному році, зберігається в окремому об'єкті. Усього таких об'єктів три - satoshi2018, satoshi2019, satoshi2020.
      
      Щоб скласти повну картину та профіль, вам необхідно об'єднати дані з цих трьох об'єктів в один об'єкт - fullProfile.
      
      Зверніть увагу, що деякі поля в об'єктах можуть повторюватися. У такому випадку в результуючому об'єкті має зберегтися значення, яке було отримано пізніше (наприклад, значення з 2020 пріоритетніше порівняно з 2019).
      
      Напишіть код, який складе повне досьє про можливу особу Сатоші Накамото. Змінювати об'єкти satoshi2018, satoshi2019, satoshi2020 не можна.`,
      `const satoshi2020 = {
        name: 'Nick',
        surname: 'Sabo',
        age: 51,
        country: 'Japan',
        birth: '1979-08-21',
        location: {
          lat: 38.869422, 
          lng: 139.876632
        }
      }
      
      const satoshi2019 = {
        name: 'Dorian',
        surname: 'Nakamoto',
        age: 44,
        hidden: true,
        country: 'USA',
        wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
        browser: 'Chrome'
      }
      
      const satoshi2018 = {
        name: 'Satoshi',
        surname: 'Nakamoto', 
        technology: 'Bitcoin',
        country: 'Japan',
        browser: 'Tor',
        birth: '1975-04-05'
      }`,
      `Завдання 5
      Дано масив книг. Вам потрібно додати до нього ще одну книгу, не змінюючи існуючий масив (в результаті операції має бути створено новий масив).`,
      `const books = [{
        name: 'Harry Potter',
        author: 'J.K. Rowling'
      }, {
        name: 'Lord of the rings',
        author: 'J.R.R. Tolkien'
      }, {
        name: 'The witcher',
        author: 'Andrzej Sapkowski'
      }];
      
      const bookToAdd = {
        name: 'Game of thrones',
        author: 'George R. R. Martin'
      }`,
      `Завдання 6
      Даний об'єкт employee. Додайте до нього властивості age і salary, не змінюючи початковий об'єкт (має бути створено новий об'єкт, який включатиме всі необхідні властивості). Виведіть новий об'єкт у консоль.
      
      const employee = {
        name: 'Vitalii',
        surname: 'Klichko'
      }`,
      `Завдання 7
      Доповніть код так, щоб він коректно працював
      
      const array = ['value', () => 'showValue'];
      
      // Допишіть код тут
      
      alert(value); // має бути виведено 'value'
      alert(showValue());  // має бути виведено 'showValue'`,
    ],
  },
  {
    text: "Домашнє завдання 4 (після зайняття 8).",
    theory: {
      text: "Теоретичні питання",
      questions: [
        "Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.",
      ],
    },
    task: [
      `
      Завдання
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
Необов'язкове завдання підвищеної складності
Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
      `,
    ],
  },
  {
    text: "Домашнє завдання 5 (після зайняття 9).",
    task: [
      `
      Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
Технічні вимоги:
При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
https://ajax.test-danit.com/api/json/users
https://ajax.test-danit.com/api/json/posts
Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/\${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.
Необов'язкове завдання підвищеної складності
Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації. Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts. Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
Додати функціонал (іконку) для редагування вмісту картки. Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/\${postId}.
      `,
    ],
  },
  {
    text: "Домашнє завдання 6 (після зайняття 10).",
    theory: {
      text: "Теоретичні питання",
      questions: [
        "Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript.",
      ],
    },
    task: [
      `
      Завдання
Написати програму "Я тебе знайду по IP"

Технічні вимоги:
Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await.
      `,
    ],
  },
  {
    text: "Домашнє завдання 7 (опціональне) (після зайняття 3).",
    task: [
      `
      Створити адаптований аналог гри Whack a mole.

      Технічні вимоги:
      Створити поле 10*10 за допомогою елемента <table>.
      Суть гри: будь-яка непідсвічена комірка в таблиці на короткий час забарвлюється в синій колір. Користувач повинен протягом відведеного часу встигнути клацнути на зафарбовану комірку. Якщо користувач встиг це зробити, вона забарвлюється зелений колір, користувач отримує 1 очко. Якщо не встиг – вона забарвлюється у червоний колір, комп'ютер отримує 1 очко.
      Гра триває доти, доки половина комірок на полі не будуть пофарбовані у зелений чи червоний колір. Як тільки це станеться, той гравець (людина чи комп'ютер), чиїх комірок на полі більше, перемагає.
      Гра повинна мати три рівні складності, що вибираються перед стартом гри:
      Легкий – нова комірка підсвічується кожні 1.5 секунди;
      Середній - нова комірка підсвічується раз на секунду;
      Важкий - нова комірка підсвічується кожні півсекунди.
      Після закінчення гри вивести на екран повідомлення про те, хто переміг.
      Після закінчення гри має бути можливість змінити рівень складності та розпочати нову гру.
      Використовувати функціонал ООП під час написання програми.
      За бажанням, замість фарбування комірок кольором, можна вставляти туди картинки.
      `,
    ],
  },
];
